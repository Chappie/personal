import React from 'react';
import Navbar from './navbar/Navbar';
import Header from './header/Header';
import MainContent from './mainContent/MainContent';
import Footer from './footer/Footer';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
      <Navbar />
      <Header />
      <MainContent />
      <Footer />
      </div>
    );
  }
}

export default App;
