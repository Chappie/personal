import React from 'react';
import Calender from '../calendar/Calendar';
import List from '../lists/List';

function Navbar() {
    const pageTitle = "Title";

    return (
        <div>
            <Calender />
            <h1>{pageTitle}</h1>
            <nav>
                <List />
            </nav>
            <main>

            </main>
        </div>
    )
}

export default Navbar;