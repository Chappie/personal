import React from 'react';
import ImageContainer from '../imageContainer/ImageContainer';
import Textbox from '../textbox/Textbox';

function MainContent() {
    return (
        <div>
            <p>main content</p>
            <ImageContainer />
            <Textbox />
        </div>
    )
}

export default MainContent;